:<<BATCH
	@echo off
	rem Windows part:
	rem Try to guess conda path and create venv
	set PATH=%PATH%;C:/ProgramData/Anaconda3/Library/bin/;C:/ProgramData/Anaconda3/Scripts/;%userprofile%/Anaconda3/condabin/;%userprofile%/Anaconda3/Library/bin/
	call conda env create -f environment_dev.yml --prefix ./local_files
	call conda activate ./local_files
	call cd ..
	call git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow
	call cd ./ml4proflow
	call pip install -e .
	pause
	exit /b
BATCH
# Deactivate base env first see bug: https://github.com/conda/conda/issues/9392
eval "$(/home/$USER/miniconda3/bin/conda shell.bash hook)"
conda env create -f environment_dev.yml --prefix ./local_files
conda activate ./local_files
cd ..
git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow
cd ./ml4proflow
pip install -e .
