# ml4proflow-standalone
An entry point for [ml4proflow](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow), a data flow oriented framework for ml applications in industry. 
`ml4proflow-standalone` manages the environment of ml4proflow including installation and updating of the framework.

# Setup
To setup the framework correctly with the required version of python and all other packages, we highly recommended to use a virtual environment. For this purpose `ml4proflow-standalone` provides an installation script. It creates a new virtual environment within this repository and installs all necessary packages in the correct version.
 
Before executing the installation script, we recommend to the create a new directory for the framework. Thus, the management between different modules is facilitated by the structure.

Inside this newly created directory, hereinafter referred to as *root_folder*, clone this repository. 
```console
$ git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-standalone
```
 
### Installation
Since ml4proflow is a customizable framework and can be deployed at different points in the production process, usability requirements differ. For this reason, `ml4proflow-standalone` offers two environments configurations by default, which can be installed via the script:
- `env-install.cmd`: Basic environment including the main framework `ml4proflow` and some basic modules [_IO_](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io) & [_Multiprocessing_](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-multiprocessing)
- `env-install-jupyter.cmd`: Extended environment including the basic environment and a frontend built on Jupyter notebooks with the module [ml4proflow-jupyter](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-jupyter)

To setup the virtual environment with all dependencies and within install the framework, execute one of the provided installation scripts: 
For installing the basic version, run `env-install.cmd` in `ml4proflow-standalone`:
```console
$ cd ./ml4proflow-standalone
$ ./env-install.cmd
``` 

For installing the extended version, run `env-install-jupyter.cmd` in `ml4proflow-standalone`:
```console
$ cd ./ml4proflow-standalone
$ ./env-install-jupyter.cmd
``` 

The installation script is platform independent and works for both operating systems, *Linux* and *Windows*.
The newly created virtual environment is located in `./local_files` of `ml4proflow-standalone`.

**Note:** It is possible to extend the basic environment with the provided frontend after the installation with `pip`. 
> Since ml4proflow is a customizable framework and can be deployed at different points in the production process, usability requirements differ. 


### Folder structure 
Going trough the steps, the environment should be setup correctly and the framework is installed. 
The folder structure should be very similiar to the following one:
``` 
root_folder
└── ml4proflow_standalone
│   └── local_files             # Contains the virtual environment
│   │   env_install.cmd         # Install script to install the virtual environment
│   │   environment.yml         # List of all package dependencies
│   │   env-start-jupyter.cmd   # Script to launch the jupyter environment
│   │   env-update.cmd          # Update script to update the virtual environment
│   │   README.md               # Readme
``` 
Adding other modules for extending the framework leads to more directories in the *root_folder* alongside `ml4proflow-standalone`.
An example folder structure is shown below: 
 
``` 
root_folder
└── ml4proflow_standalone
│   └── local_files             # Contains the virtual environment
│   │   env_install.cmd         # Install script to install the virtual environment
│   │   environment.yml         # List of all package dependencies
│   │   env-start-jupyter.cmd   # Script to launch the jupyter environment
│   │   env-update.cmd          # Update script to update the virtual environment
│   │   README.md               # Readme
│   
└── ml4proflow-mods-xxx
│   └── docs                    # Contains documentation
│   └── notebooks               # Contains jupyter notebooks
│   └── src/ml4proflow_mods/xxx # Contains source code
│   └── test                    # Contains tests
│   │   LICENSE.txt             # The license
│   │   README.md               # Readme
│   │   setup.py                # Setup script to install this package
```

# Usage
### Basic environment
To use the abilities of the framework without *JupyterLab*, navigate in the folder of `ml4proflow-standalone` and activate the virtual environment: 

On *Linux*:
```console 
$ source my_env/local_files/bin/activate
``` 
On *Windows*:
```console 
$ conda activate my_env
```
### Extended environment 
To use the framework inside juppyter notebooks, run the provided start script `env-start-jupyter.cmd`. This will activate the virtual environment in `./local_files` and will open *JupyterLab* at `.local_files/ml4proflow` automatically in your browser.
```console 
$ ./env-start-jupyter.cmd
``` 

# Update the environment
If the environment is broken and a re-installation is necessary, or it simply needs to be brought up to date, check out the newest version of this repository and run the provided update-script:
```console
$ cd ./ml4proflow-standalone
$ git pull
$ ./env-update.cmd
```

# Documentation? 
