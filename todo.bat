@set PATH=%PATH%;C:/ProgramData/Anaconda3/Library/bin/;C:/ProgramData/Anaconda3/Scripts/;%userprofile%/Anaconda3/condabin/;%userprofile%/Anaconda3/Scripts/
set FOLDER=%CD%
call conda activate ./local_files
cd %userprofile%/.node-red/
call npm uninstall toolbox
call npm install "%FOLDER%"/node-red/
cd %FOLDER%
CHCP 65001
call node-red
pause