:<<BATCH
	@echo off
	rem Windows part:
	rem Try to guess conda path and create venv
	set PATH=%PATH%;C:/ProgramData/Anaconda3/Library/bin/;C:/ProgramData/Anaconda3/Scripts/;%userprofile%/Anaconda3/condabin/;%userprofile%/Anaconda3/Library/bin/
	call conda env create -f environment_dev.yml --prefix ./local_files
	call conda activate ./local_files
	call cd ..
	call git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow
	call git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io
	call git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-jupyter
	call git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-amiro
	call git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-ina
	rem call git clone https://github.com/cklarhorst/ml4proflow-mods-ml4pro.git
	call pip install -e ./ml4proflow
	call pip install -e ./ml4proflow-mods-io
	call pip install -e ./ml4proflow-jupyter
	call pip install -e ./ml4proflow-mods-amiro
	call pip install -e ./ml4proflow-mods-ina
	rem call cd ../ml4proflow-mods-ml4pro
	rem call pip install -e .
	pause
	exit /b
BATCH
# Deactivate base env first see bug: https://github.com/conda/conda/issues/9392 
# TODO: Update linux gits!
eval "$(/home/$USER/miniconda3/bin/conda shell.bash hook)"
conda env create -f environment_dev.yml --prefix ./local_files
conda activate ./local_files
cd ..
git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow
git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io
git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-jupyter
#git clone https://github.com/cklarhorst/ml4proflow-mods-ml4pro.git
cd ./ml4proflow
pip install -e .
cd ../ml4proflow-mods-io
pip install -e .
cd ../ml4proflow-jupyter
pip install -e .
#cd ../ml4proflow-mods-ml4pro
#pip install -e .
