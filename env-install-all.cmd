:<<BATCH
	@echo off
	rem Windows part:
	rem Try to guess conda path and create venv
	set PATH=%PATH%;C:/ProgramData/Anaconda3/Library/bin/;C:/ProgramData/Anaconda3/Scripts/;%userprofile%/Anaconda3/condabin/;%userprofile%/Anaconda3/Library/bin/
	call conda env create -f environment_all.yml --prefix ./local_files
	pause
	exit /b
BATCH
conda env create -f environment_all.yml --prefix ./local_files