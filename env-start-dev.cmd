:<<BATCH
	@echo off
	rem Windows part:
	rem Try to guess conda path and create venv
	set PATH=%PATH%;C:/ProgramData/Anaconda3/Library/bin/;C:/ProgramData/Anaconda3/Scripts/;%userprofile%/Anaconda3/condabin/;%userprofile%/Anaconda3/Library/bin/
	call conda activate ./local_files
	cd ../ml4proflow-jupyter/notebooks
	call jupyter lab --LabApp.default_url='/lab/workspaces/lab?reset'
	pause
	exit /b
BATCH
conda activate ./local_files
cd ../ml4proflow-jupyter/notebooks
jupyter lab --LabApp.default_url='/lab/workspaces/lab?reset'